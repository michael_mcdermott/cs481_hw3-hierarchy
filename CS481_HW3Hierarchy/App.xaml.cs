﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace CS481_HW3Hierarchy
{
    public partial class App : Application
    {

        /*public static booleans are used set to false in when the app is launched
         * as each page is visited, the page will set individual pagevisited boolean
         * to "true" in its PageAppearing method.
         */
        public static Boolean hobbiesPagevisited { get; set; }
        public static Boolean bioPagevisited { get; set; }
        public static Boolean professionalPagevisited { get; set; }
        public App()
        {
            hobbiesPagevisited = false;
            bioPagevisited = false;
            professionalPagevisited = false;
            InitializeComponent();

            MainPage = new NavigationPage(new MainPage());   
    }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
