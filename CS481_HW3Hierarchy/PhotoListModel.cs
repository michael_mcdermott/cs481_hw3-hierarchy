﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Windows.Input;
using Xamarin.Forms;

namespace CS481_HW3Hierarchy
{

    /*PhotoListModel is a model created for the use in a Carousel
     *It is a List of PhotoClass objects
     * When instantiated, it adds new PhotoClass objects to the list,
     * and each object is isntantiated with a string that is the photos filename
     * Then the list is turned into an Observable Collection, which is needed by
     * the Carousel
     */
    public class PhotoListModel 
    {
        readonly IList<PhotoClass> source;

        public ObservableCollection<PhotoClass> PhotoList { get; private set; }

        public PhotoListModel()
        {
            source = new List<PhotoClass>();
            source.Add(new PhotoClass { PhotoPath = "felina.jpeg" });
            source.Add(new PhotoClass { PhotoPath = "studio.jpeg" });
            source.Add(new PhotoClass { PhotoPath = "desk.JPG" });
           

            PhotoList = new ObservableCollection<PhotoClass>(source);

        }
    }
}

