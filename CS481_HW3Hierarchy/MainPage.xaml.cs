﻿using System.ComponentModel;
using Xamarin.Forms;

namespace CS481_HW3Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }

       void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {

        }

        void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {

        }

       async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Button button = (Button)sender;

            if(button.Text == "Bio")
            {
                await Navigation.PushAsync(new BioPage());
            }
            else if(button.Text == "Hobbies")
            {
                await Navigation.PushAsync(new HobbiesPage());
            }
            else if(button.Text == "Professional")
            {
                await Navigation.PushAsync(new ProfessionalPage());
            }

        }

    }
}
