﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace CS481_HW3Hierarchy
{
    public partial class BioPage : ContentPage
    {
        public BioPage()
        {
            InitializeComponent();
        }

  /* Button_Clicked is a single function that will handle any of the navigation
   * buttons to other pages by reading the text in the button and then
   * navigating to the appropriate page
   */
        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            Button button = (Button)sender;

            if (button.Text == "Bio")
            {
                await Navigation.PushAsync(new BioPage());
            }
            else if (button.Text == "Hobbies")
            {
                await Navigation.PushAsync(new HobbiesPage());
            }
            else if (button.Text == "Professional")
            {
                await Navigation.PushAsync(new ProfessionalPage());
            }

        }

        //navigates to home page used popToRoot, it will clear the stack
        async void GoHome(System.Object sender, System.EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        //sets the visited boolean to true when the page launches
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            App.bioPagevisited = true;
        }

        //if youre navigating away from the page and all pages have been visited, it will tell you
        async void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            if (App.bioPagevisited == true && App.hobbiesPagevisited == true && App.professionalPagevisited == true)
            {
                await DisplayAlert("Done", "You have gone through all the pages in the app", "OK?").ConfigureAwait(false);
                App.hobbiesPagevisited = false; App.bioPagevisited = false; App.professionalPagevisited = false;
            }
        }

    }
}
