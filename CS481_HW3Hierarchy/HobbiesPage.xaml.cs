﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;
using Xamarin.Essentials;

namespace CS481_HW3Hierarchy
{
    public partial class HobbiesPage : ContentPage
    {
        //URI is used for the spotify link that opens in the browser
        Uri uri = new Uri("https://open.spotify.com/album/4jsr2MPhEdDk5eYUakagzP?si=N9eDhlUXQbSoaTAY_WUgHg");

        public HobbiesPage()
        {
            InitializeComponent();
        }


        /* Button_Clicked is a single function that will handle any of the navigation
         * buttons to other pages by reading the text in the button and then
         * navigating to the appropriate page
         */
        async void Button_Clicked(System.Object sender, System.EventArgs e)
        {
            //makes a button from the object passed in
            Button button = (Button)sender;

            //handles navigating to the right page based on the text in the button
            if (button.Text == "Bio")
            {
                await Navigation.PushAsync(new BioPage());
            }
            else if (button.Text == "Hobbies")
            {
                await Navigation.PushAsync(new HobbiesPage());
            }
            else if (button.Text == "Professional")
            {
                await Navigation.PushAsync(new ProfessionalPage());
            }

        }

        //navigates to home page used popToRoot, it will clear the stack
        async void GoHome(System.Object sender, System.EventArgs e)
        {
            await Navigation.PopToRootAsync();
        }

        //sets the visited boolean to true when the page launches
        void ContentPage_Appearing(System.Object sender, System.EventArgs e)
        {
            App.hobbiesPagevisited = true;

        }


        //if youre navigating away from the page and all pages have been visited, it will tell you
       async void ContentPage_Disappearing(System.Object sender, System.EventArgs e)
        {
            if (App.bioPagevisited == true && App.hobbiesPagevisited == true && App.professionalPagevisited == true)
            {
                await DisplayAlert("Done", "You have gone through all the pages in the app", "OK?").ConfigureAwait(false);
                App.hobbiesPagevisited = false; App.bioPagevisited = false; App.professionalPagevisited = false;
            }
        }


        //when the spotify button is clicked, this opens a browser with the link to the song
        void ImageButton_Clicked(System.Object sender, System.EventArgs e)
        {
            OpenBrowser(uri);
        }
        public async void OpenBrowser(Uri uri)
        {
            await Browser.OpenAsync(uri, BrowserLaunchMode.SystemPreferred);
        }
    }
}
