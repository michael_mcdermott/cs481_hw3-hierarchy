﻿using System;
namespace CS481_HW3Hierarchy
{
    /*Very simple class that just stores a string in its object, since I couldn't
     * create a model that has a list of strings. It has to be an object that
     * has a variable that can be used for binding. 
     */
    public class PhotoClass
    {
        public string PhotoPath { get; set; }
    }
}
